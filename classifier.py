import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils import data
import pandas as pd
import numpy as np


class Data_set(data.Dataset):
    def __init__(self, filename="training_data.csv", eval=False):
        super().__init__()
        self.eval = eval
        self.columns = ["f1", "f2", "f3", "target"]
        self.f3_encoding = ["A", "B", "D", "E"]
        self.df = pd.read_csv(filename, names=self.columns, header=1)
        self.check_data()

    def check_data(self):
        if self.df.isnull().values.any():
            print("Warning removing entries containing nan values")
            self.df.dropna(inplace=True)

        assert self.df.shape[0] > 1, "data is empty"

        for i in range(1, self.df.shape[0]):
            row_df = self.df.iloc[i]
            assert type(row_df["f1"]) is np.float64, "f1, line " + str(i) + " is not float"
            assert type(row_df["f2"]) is np.float64, "f2, line " + str(i) + " is not float"
            assert row_df["f3"] in self.f3_encoding, "f3, line " + str(i) + " is invalid"
            assert row_df["target"] in [0, 1, 2], "invalid target" + str(row_df["target"])

    def __len__(self):
        if self.eval:
            return self.df.shape[0] - self.df.shape[0] // 10 * 9

        return self.df.shape[0] // 10 * 9

    def __getitem__(self, item):
        if self.eval:
            row_df = self.df.iloc[item + self.df.shape[0] // 10 * 9]
        else:
            row_df = self.df.iloc[item]

        x = torch.zeros([6])
        x[0] = row_df["f1"]
        x[1] = row_df["f2"]
        x[0:2] = torch.sigmoid(x[0:2])

        x[self.f3_encoding.index(row_df["f3"]) + 2] = 1
        y = row_df["target"]
        return x, y


class Net(nn.Module):
    def __init__(self):
        super().__init__()
        self.fcl1 = nn.Linear(6, 5)
        self.fcl2 = nn.Linear(5, 3)

    def forward(self, x):
        x = F.leaky_relu(self.fcl1(x))
        x = self.fcl2(x)
        return x


class Util:

    def __init__(self, load_net_path=None):
        if load_net_path:
            with open(load_net_path, 'rb') as f:
                self.net = torch.load(f)
        else:
            self.net = Net()

    def train(self, save_path, epochs, batch_size=100, lr=3e-4):

        try:
            with open(save_path, 'x') as _:
                pass
        except OSError as e:
            print("Save location warning", e)

        data_set = Data_set()
        data_loader = data.DataLoader(data_set, batch_size=batch_size, num_workers=4)
        opt = torch.optim.Adam(self.net.parameters(), lr)
        loss_fn = nn.CrossEntropyLoss()

        for e in range(epochs):
            for b_id, (x, y) in enumerate(data_loader):
                yi = self.net.forward(x)
                loss = loss_fn(yi, y)
                loss.backward()
                opt.step()
                opt.zero_grad()

            if e % 10 == 0:
                print("epoch:", e, "loss:", loss.item())

        with open(save_path, 'wb') as f:
            torch.save(self.net, f)

    def test(self):
        accuracy = 0
        data_set = Data_set(eval=True)
        data_loader = data.DataLoader(data_set, batch_size=500, num_workers=4)
        for b_id, (x, y) in enumerate(data_loader):
            yi = self.net.forward(x)
            maxes = yi.argmax(1)
            correct = maxes.long() == y.long()
            accuracy += correct.float().sum().item()

        return accuracy / data_set.__len__()

    def classify(self, f1, f2, f3):
        x = torch.zeros([6])
        x[0], x[1] = f1, f2
        x[0:2] = torch.sigmoid(x[0:2])
        x[["A", "B", "D", "E"].index(f3) + 2] = 1
        y = self.net.forward(x.view(1, 6))
        y = y.argmax()
        return int(y)


if __name__ == '__main__':
    u = Util()
    u.train("test_net", 40)
    print("Accuracy:", u.test())
