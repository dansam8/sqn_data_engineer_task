

#Interview Test

A simple classifier with a flask API.

##Installation

 ```bash
 git clone https://dansam8@bitbucket.org/dansam8/sqn_data_engineer_task.git
 cd inteview sqn_data_engineer_task
 pip install -r requirements.txt
 ```
 
 ##Usage
 ###Run
 ```bash
 python3 app.py
 ```
 Include optional argument `--init_db` to initialise the sqlite database
 
###Interact
The application has two endpoints: `/classify` and `/stats`.
The first endpoint `/classify` accepts GET requests with three parameters f1, f2, and f3 (the features used in the training data). For example:
```bash
https://localhost:5000/classify?f3=A&f1=0.23&f2=0.4
```
The application returns the following `JSON` response:

```json
{"predicted_class": 1, "status": "OK"}
```

If the `predicted_class` is the same as the prediction in two most recently logged requests,
the response `status` shall be: `WARNING`.

```json
{"predicted_class": 0, "status": "WARNING"}
```

If the request is not valid (e.g. wrong parameters), the response shall contain `error_message` and its
`status` shall be `ERROR`.

```json
{"status": "ERROR", "error_message": "f1 should be float"}
```

The second endpoint `/stats` accepts `GET` requests with no parameters:

```bash
https://localhost:5000/stats
```

The response contains statistics for the logged classification requests with `status=OK`:
mean values for `f1`, `f2`, and the most frequent value for `f3`. Example:

```json
{"mean_f1": 0.25, "mean_f2": 0.33, "most_frequent_f3": "A"}
```
