from flask import Flask, request
import classifier
import sqlite3
import time
import argparse
import os

from classifier import Net  # needed due to namespace issue with pickle (when loading the net from this scope)

DB_PATH = "data.db"
NEURAL_NET_PATH = "test_net"

app = Flask(__name__)
clas = classifier.Util(NEURAL_NET_PATH)

@app.route("/classify")
def classify():
    response = {}

    for _ in [0]:

        # check input
        fail = False
        for arg in ["f1", "f2", "f3"]:
            if arg not in request.args:
                response.update({"status": "ERROR", "error_message": "Expected arg " + arg + ", was not given."})
                fail = True
        if fail:
            break

        f1, f2, f3 = request.args.get('f1'), request.args.get('f2'), request.args.get('f3')

        if f3 not in ["A", "B", "D", "E"]:
            response.update({"status": "ERROR", "error_message": "Expected f3 to be ether: A, B, D or E"})
            break

        try:
            f1 = float(f1)
        except ValueError:
            response.update({"status": "ERROR", "error_message": "f1 should be float"})
            break

        try:
            f2 = float(f2)
        except ValueError:
            response.update({"status": "ERROR", "error_message": "f2 should be float"})
            break
        # /check input

        # classify
        response["predicted_class"] = clas.classify(f1, f2, f3)
        response["status"] = "OK"

    # log
    sql_conn = sqlite3.connect(DB_PATH)
    sql_cursor = sql_conn.cursor()

    max_request_id = sql_cursor.execute("SELECT MAX(id_request) FROM classification_requests").fetchone()[0]
    current_request_id = max_request_id + 1 if max_request_id is not None else 1
    entry = {
        "id_request": current_request_id,
        "request_timestamp": str(time.time()),
        "predicted_class": response["predicted_class"] if "predicted_class" in response else None,
        "response_status": response["status"],
        "error_message": response["error_message"] if "error_message" in response else None
    }
    sql_cursor.execute("INSERT INTO classification_requests VALUES "
                       "(:id_request, :request_timestamp, :predicted_class, :response_status, :error_message)",
                       entry)

    if response["status"] == "OK":
        max_request_param_id = sql_cursor.execute("SELECT MAX(id_request_param) FROM classification_request_params").fetchone()[0]
        entry_params = {
            "id_request_param": max_request_param_id + 1 if max_request_param_id is not None else 1,
            "id_request": current_request_id,
            "f1": f1,
            "f2": f2,
            "f3": f3
        }
        sql_cursor.execute("INSERT INTO classification_request_params VALUES "
                           "(:id_request_param, :id_request, :f1, :f2, :f3 ) ",
                           entry_params)
    # /log
    # warn if three consecutive predictions are the same
    if response["status"] == "OK":
        last_predicted_classes = sql_cursor.execute("""
                                    SELECT predicted_class 
                                    FROM classification_requests 
                                    ORDER BY id_request DESC""").fetchmany(3)

        if 2 == sum([int(x[0] == last_predicted_classes[0][0]) for x in last_predicted_classes[1:]]):
            response["status"] = "WARNING"

    sql_conn.commit()
    sql_conn.close()
    return response


@app.route("/stats")
def stats():
    sql_conn = sqlite3.connect(DB_PATH)
    sql_cursor = sql_conn.cursor()

    if sql_cursor.execute("SELECT COUNT(*) FROM classification_request_params").fetchone()[0] == 0:
        return {"error": "table is empty"}

    stats_json = {
        "mean_f1": sql_cursor.execute("SELECT AVG(f1) FROM classification_request_params").fetchone()[0],
        "mean_f2": sql_cursor.execute("SELECT AVG(f2) FROM classification_request_params").fetchone()[0],
        "most_frequent_f3": sql_cursor.execute("""
            SELECT f3,
            COUNT(f3) AS occurrences
            FROM        classification_request_params
            GROUP BY    f3
            ORDER BY    occurrences DESC""").fetchone()[0],
        "status": "OK"
    }
    sql_conn.close()
    return stats_json


def init_db():
    if os.path.exists(DB_PATH):
        print("Error: database file already exists at", DB_PATH, "remove this before initialising the database")
        exit(0)

    sql_conn = sqlite3.connect(DB_PATH)
    sql_cursor = sql_conn.cursor()

    sql_cursor.execute(
        """CREATE TABLE classification_requests
        (
            id_request INTEGER PRIMARY KEY,
            request_timestamp TEXT NOT NULL,
            predicted_class INTEGER,
            response_status TEXT NOT NULL,
            error_message TEXT
        );"""
    )

    sql_cursor.execute(
        """CREATE TABLE classification_request_params
        (
            id_request_param INTEGER PRIMARY KEY,
            id_request INTEGER,
            f1 TEXT FLOAT NULL,
            f2 TEXT FLOAT NULL,
            f3 TEXT NOT NULL,
            FOREIGN KEY(id_request) REFERENCES classification_requests(id_request)
        )"""
    )

    sql_conn.close()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--init_db", action="store_true", help=
                        "Initialises sqlite database")
    args = parser.parse_args()
    if args.init_db:
        init_db()

    app.run(ssl_context="adhoc")


if __name__ == '__main__':
    main()


